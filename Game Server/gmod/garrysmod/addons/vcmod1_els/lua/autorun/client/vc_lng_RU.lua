// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.
local Lng = {}

//You do not have to translate everything. Things that are not translated will be written in English.

//Set up the language
Lng.Name = "Русский"
Lng.Language_Code = "RU"
Lng.Translated_By_Name = "Progeny"
Lng.Translated_By_Link = "http://steamcommunity.com/id/progenyjez/"
Lng.Translated_Date = "2015 03 17"

////////////// Translation starts here

////////GENERAL
Lng.Locked = "Транспорт закрыт"
Lng.UnLocked = "Транспорт открыт."
Lng.Chat = "Для регулировки света и вида, введите '!vcmod' в чате."
Lng.Broken = "Это транспортное средство сломано и нуждается в ремонте."
Lng.Trl_Atch = "Прицеп прикреплен."
Lng.Trl_Detch = "Прицеп отсоединен."
Lng.ELS_TuningIntoPoliceC = "Настроиться на полицейский радиоэфир."
Lng.ELS_NoPoliceRCFound = "Полицейский радиоэфир не был найден."

////////Pickup
Lng.TouchCar100 = "Коснитесь авто для 100% ремонта."
Lng.TouchCar25 = "Коснитесь авто для ремонта на +25% от первоначального здоровья."
Lng.TouchCar10 = "Коснитесь авто для ремонта на +10% от первоначального здоровья."

////////MENU general
Lng.Info = "Информация"
Lng.Menu = "Меню"
Lng.Language = "Русский"
Lng.Personal = "Персональные"
Lng.Administrator = "Администратор"
Lng.Options = "Настройки"
Lng.Main = "Главное"
Lng.Controls = "Управление"
Lng.HUD = "HUD"
Lng.View = "Вид"
Lng.Radio = "Радио"
Lng.Multiplier = "Множитель"
Lng.OptOnly_You = "Данные настройки влияют только на вас"
Lng.OptOnly_Admin = "Данные настройки могут быть изменены только администратором"
Lng.NPC_Settings = "Настройки NPC"
Lng.Height = "Высота"
Lng.FadeOutDistance = "Дистанция исчезновения"
Lng.Reset = "Сбросить"
Lng.Save = "Сохранить"
Lng.Load = "Загрузить"
Lng.Volume = "Громкость"
Lng.Distance = "Дистанция"
Lng.None = "Ничего"
Lng.EnterKey = "Нажмите клавишу"

Lng.Enabled_Cl = "Клиенсткая часть активирована"
Lng.Enabled_Sv = "Серверная часть активирована"

Lng.Lights = "Освещение"
Lng.Health = "Жизнь"
Lng.Sound = "Звук"
Lng.Other = "Другое"
Lng.CreatedBy = "Создал"
Lng.Enabled = "Активирован"
Lng.OffTime = "Выключить время"
Lng.Time = "Время"
Lng.DistMultiplier = "Множитель расстояния"
Lng.ControlsReset = "Управление сброшено."
Lng.SettingsSaved = "Настройки сохранены."
Lng.SettingsReset = "Настройки сброшены."
Lng.LoadedSettingsFromServer = "Настройки загружены с сервера."
Lng.InteriorIndicators = "Внутренние индикаторы"
Lng.ExtraGlow = "Дополнительное свечение"

////////MENU ELS
Lng.ManulSiren = "Ручная сирена"
Lng.ELS_SirenSwitch = "ELS переключатель сирены"
Lng.ELS_SirenToggle = "ELS переключение сирены"
Lng.ELS_LightsSwitch = "ELS переключатель света"
Lng.ELS_LightsToggle = "ELS переключение света"
Lng.VCModMainEnabled = "VCMod main включен"
Lng.VCModELSEnabled = "VCMod ELS включен"
Lng.ELSLightsEnabled = "ELS освещение включено"
Lng.AutoDisableELSLights = "Авто-выключение ELS освещения"
Lng.OffOnExit = "Выключение при выходе"
Lng.Siren = "Сирена"
Lng.AutoDisableELSSounds = "Авто-выключение ELS сигналов"
Lng.Manual = "Ручной"
Lng.Bullhorn = "Мегафон"
Lng.PoliceChatterEnabled = "Полиейский радиоэфир активирован"
Lng.PoliceChatter = "Полицейский радиоэфир"
Lng.PoliceChatter_Info = "Полицейский радиоэфир - это действительный полицеский канал передающийся через радио."
Lng.SelectedRadioChatter = "Выбранная радио частота"
Lng.ReduceDamageToEmergencyVehicles = "Уменьшить повреждения для экстренного транпорта"

////////MENU personal info
Lng.YouAreUsingVCMod = "Вы используете VCMod"
Lng.ServerIsUsingVCMod = "Данный сервер использует VCMod"
Lng.Info_EverThought = "Вы когда-нибудь думали, что транспорт в Garry's Mod не реалистичен или ему не хватает детальности?\nVCMod создан для того, чтобы сделать транспорт в Garry's Mod таким же хорошим, как и в других играх."
Lng.Info_VCModHasFollowingAddons = "VCMod имеет следующие дополнения:"

////////MENU personal options
Lng.VisDist = "Дистанция видимости"
Lng.Warmth = "Теплота"
Lng.Lines = "Линии"
Lng.Glow = "Свечение"
Lng.DynamicLights = "Динамичное освещение"
Lng.ThirdPView = "Вид от третьего лица"
Lng.DynamicView = "Динамичный вид"
Lng.AutoFocus = "Авто-фокус"
Lng.Reverse = "Обратно"
Lng.VectorStiffness = "Вектор упругости %"
Lng.AngleStiffness = "Угол упругости %"
Lng.IgnoreWorld = "Игнорирование мира"
Lng.TruckView = "Вид при присоединенном прицепе"

////////MENU controls
Lng.HoldDuration = "Длительность задержки"
Lng.Mouse = "Мышь"
Lng.KeyboardInput = "Ввод от клавиатуры"
Lng.MouseInput = "Ввод от мыши"

Lng.NightLights = "Ночной свет"
Lng.HeadLights = "Передний свет"
Lng.LowHigh = "Переключение ближних/дальних фар"
Lng.HazardLights = "Аварийный сигнал"
Lng.BlinkerLeft = "Левый поворотный сигнал"
Lng.BlinkerRight = "Правый поворотный сигнал"
Lng.Horn = "Звуковой сигнал"
Lng.Cruise = "Круиз"
Lng.LockUnlock = "Закрыть/открыть"
Lng.LookBehind = "Посмотреть назад"
Lng.DetachTrl = "Отцепить прицеп"

////////MENU hud
Lng.Effect3D = "3D эффект"
Lng.HUDHeight = "Высота богового HUD %"
Lng.HUD_Name = "Имя"
Lng.HUD_Icons = "Символы"
Lng.HUD_Cruise = "Круиз"
Lng.HUD_Cruise_MPH = "Ми/ч вместо км/ч"
Lng.HUD_Repair = "Ремонт"
Lng.HUD_ELS_Siren = "ELS сирена"
Lng.HUD_ELS_Lights = "ELS свет"

////////MENU admin options
Lng.HandbrakeLights = "Стоп-сигнал"
Lng.InteriorLights = "Внутреннее освещение"
Lng.BlinkersOffExit = "Выключение поворотных сигналов при выходе"

Lng.DamageEnabled = "Включить повреждения"
Lng.StartHealthMultiplier = "Старт множителя жизни"
Lng.PhysicalDamage = "Физическое повреждение"
Lng.FireDuration = "Длительность огня"
Lng.RemoveCarAfterExplosion = "Убрать автомобиль после взрыва"
Lng.ReducePlayerDmgWhileInCar = "Уменьшить повреждения игрока в автомобиле"

Lng.DoorSounds = "Звук дверей"
Lng.TruckRevBeep = "Звук прицепа при обратном движении"

Lng.SteeringWheelLOnExit = "Блокировка руля при выходе"
Lng.BrakesLOnExit = "Блокировка тормозов при выходы"
Lng.WheelDust = "Пыль от колёс"
Lng.WheelDustWhileBraking = "Пыль от колёс при торможении"
Lng.MatchPlayerSpeedExit = "Сопоставить скорость игрока при выходе"
Lng.NoCollidePlyOnExit = "Отключить столкновение игрока с автомобилем при выходе"
Lng.Exhaust = "Выхлопы"
Lng.PassengerSeats = "Пассажирские места"
Lng.TrailerAttach = "Прикрепление прицепа"
Lng.TrailerAttachConStrengthM = "Множитель силы присоединения прицепа X"
Lng.TrailersCanAtchToReg = "Прикрепление прицепа к обычным автомобилям"
Lng.RepairToolSpeedMult = "Множитель скорости инструмента для ремонта"

if !VC_Lng_T then VC_Lng_T = {} end VC_Lng_T[Lng.Language_Code] = Lng