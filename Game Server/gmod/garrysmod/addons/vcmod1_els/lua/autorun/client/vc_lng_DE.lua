// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.
local Lng = {}

//You do not have to translate everything. Things that are not translated will be written in English.

//Set up the language
Lng.Name = "Deutsch"
Lng.Language_Code = "DE"
Lng.Translated_By_Name = "GreenAurora! ;)"
Lng.Translated_By_Link = "http://steamcommunity.com/id/nVrAm/"
Lng.Translated_Date = "2015 03 16"

////////////// Translation starts here

////////GENERAL
Lng.Locked = "Fahrzeug abgeschlossen!"
Lng.UnLocked = "Fahrzeug geöffnet!"
Lng.Chat = 'Um fahrzeugbezogene Einstellungen (Scheinwerfer, Sicht) zu ändern, schreibe "!vcmod" in den Chat.'
Lng.Broken = "Dieses Fahrzeug ist defekt und muss vor seiner Benutzung repariert werden."
Lng.Trl_Atch = "Anhänger angekoppelt."
Lng.Trl_Detch = "Anhänger abgekoppelt."
Lng.ELS_TuningIntoPoliceC = "Suche nach Polizeifunk-Kanal.."
Lng.ELS_NoPoliceRCFound = "Es wurde kein Funkkanal gefunden!"

////////Pickup
Lng.TouchCar100 = "Berühre ein Fahrzeug, um es vollständig zu reparieren."
Lng.TouchCar25 = "Berühre ein Fahrzeug, um es zu 25% zu reparieren."
Lng.TouchCar10 = "Berühre ein Fahrzeug, um es zu 10% zu reparieren."

////////MENU general
Lng.Info = "Infos"
Lng.Menu = "Menu"
Lng.Language = "Sprache"
Lng.Personal = "Client"
Lng.Administrator = "Administrator"
Lng.Options = "Optionen"
Lng.ELSOptions = "ELS Optionen"
Lng.Main = "Haupt"
Lng.Controls = "Tastenbelegung"
Lng.HUD = "HUD"
Lng.View = "Sicht"
Lng.Radio = "Radio"
Lng.Multiplier = "Multiplikator"
Lng.OptOnly_You = "Diese Einstellungen betreffen nur dich"
Lng.OptOnly_Admin = "Diese Einstellungen können nur von einem Administrator geändert werden"
Lng.NPC_Settings = "NPC Optionen"
Lng.Height = "Höhe"
Lng.FadeOutDistance = "Ausblende-Distanz"
Lng.Reset = "Reset"
Lng.Save = "Speichern"
Lng.Load = "Laden"
Lng.Volume = "Lautstärke"
Lng.Distance = "Distanz"
Lng.None = "Nichts"
Lng.EnterKey = "Taste zum Einsteigen"

Lng.Enabled_Cl = "Clientside aktiviert!"
Lng.Enabled_Sv = "Serverside aktiviert!"

Lng.Lights = "Lichter"
Lng.Health = "Status"
Lng.Sound = "Sound"
Lng.Other = "Andere"
Lng.CreatedBy = "Erstellt von"
Lng.Enabled = "Aktiviert"
Lng.OffTime = "Zeit bis zum Ausschalten"
Lng.Time = "Zeit"
Lng.DistMultiplier = "Distanz Multiplikator"
Lng.ControlsReset = "Tastenbelegung zurückgesetzt."
Lng.SettingsSaved = "Einstellungen gespeichert."
Lng.SettingsReset = "Einstellungen zurückgesetzt."
Lng.LoadedSettingsFromServer = "Einstellungen wurden vom Server geladen."
Lng.InteriorIndicators = "Innenanzeigen"
Lng.ExtraGlow = "Zusätzliches Glühen (Licht)"

////////MENU ELS
Lng.ManulSiren = "Manuelle Sirene"
Lng.ELS_SirenSwitch = "ELS Sirene umschalten"
Lng.ELS_SirenToggle = "ELS Sirene benutzen"
Lng.ELS_LightsSwitch = "ELS Lichter umschalten"
Lng.ELS_LightsToggle = "ELS Lichter benutzen"
Lng.VCModMainEnabled = "VCMod main aktiviert"
Lng.VCModELSEnabled = "VCMod ELS aktiviert"
Lng.ELSLightsEnabled = "ELS Lichter aktiviert"
Lng.AutoDisableELSLights = "ELS-Lichter automatisch ausschalten"
Lng.OffOnExit = "Beim Aussteigen deaktivieren."
Lng.Siren = "Sirene"
Lng.AutoDisableELSSounds = "ELS-Sound automatisch ausschalten"
Lng.Manual = "Manuell"
Lng.Bullhorn = "Horn"
Lng.PoliceChatterEnabled = "Polizeifunk angeschaltet."
Lng.PoliceChatter = "Polizeifunk"
Lng.PoliceChatter_Info = "Der Polizeifunk ist Live-Polizeifunk aus verschiedenen Onlineradios."
Lng.SelectedRadioChatter = "Gewählter Funk-Kanal:"
Lng.ReduceDamageToEmergencyVehicles = "Schaden an Einsatzfahrzeugen reduzieren."

////////MENU personal info
Lng.YouAreUsingVCMod = "Du benutzt VCMod."
Lng.ServerIsUsingVCMod = "Dieser Server benutzt VCMod."
Lng.Info_EverThought = "Wusstest du, dass die Fahrzeuge in Garry's Mod sehr unrealistisch wirken?\nVCMod wurde geschaffen, damit die Fahrzeuge so realtistisch wie in anderen Spielen aussehen."
Lng.Info_VCModHasFollowingAddons = "Von VCMod gibt es die folgenden Versionen:"

////////MENU personal options
Lng.VisDist = "Sichtbarkeitsdistanz"
Lng.Warmth = "Wärme (Licht)"
Lng.Lines = "Lines"
Lng.Glow = "Glühen (Licht)"
Lng.DynamicLights = "Dynamische Lichter"
Lng.ThirdPView = "Third person Sicht"
Lng.DynamicView = "Dynamische Sicht"
Lng.AutoFocus = "Autofokus"
Lng.Reverse = "Rückwärts"
Lng.VectorStiffness = "Vector steifheit %"
Lng.AngleStiffness = "Angle steifheit %"
Lng.IgnoreWorld = "Welt ignorieren."
Lng.TruckView = "LWK-Anhänger Ansicht"

////////MENU controls
Lng.HoldDuration = "Haltelänge"
Lng.Mouse = "Maus"
Lng.KeyboardInput = "Tastatur Eingabe"
Lng.MouseInput = "Maus Eingabe"

Lng.NightLights = "Nachtlichter"
Lng.HeadLights = "Frontlichter"
Lng.LowHigh = "Fern & Abblendlicht"
Lng.HazardLights = "Warnblinkanlage"
Lng.BlinkerLeft = "Blinker Links"
Lng.BlinkerRight = "Blinker Rechts"
Lng.Horn = "Hupe"
Lng.Cruise = "Tempomat"
Lng.LockUnlock = "Schließen/Öffnen"
Lng.LookBehind = "Nach Hinten sehen."
Lng.DetachTrl = "Anhänger abkoppeln."

////////MENU hud
Lng.Effect3D = "3D Effekt"
Lng.HUDHeight = "Seiten-HUD Höhe %"
Lng.HUD_Name = "Name"
Lng.HUD_Icons = "Icons"
Lng.HUD_Cruise = "Tempomat"
Lng.HUD_Cruise_MPH = "mph anstatt km/h"
Lng.HUD_Repair = "Reparieren"
Lng.HUD_ELS_Siren = "ELS Sirene"
Lng.HUD_ELS_Lights = "ELS Lichter"

////////MENU admin options
Lng.HandbrakeLights = "Handbremsenlichter"
Lng.InteriorLights = "Innenbeleuchtung"
Lng.BlinkersOffExit = "Blinker beim Aussteigen ausschalten."

Lng.DamageEnabled = "Schaden"
Lng.StartHealthMultiplier = "Fahrzeuggesundheits multiplikator"
Lng.PhysicalDamage = "Physikalischer Schaden"
Lng.FireDuration = "Dauer des Feuers"
Lng.RemoveCarAfterExplosion = "Fahrzeug nach explosion entfernen."
Lng.ReducePlayerDmgWhileInCar = "Spieler-Schaden in einem Fahrzeug reduzieren."

Lng.DoorSounds = "Tür Sounds"
Lng.TruckRevBeep = "LKW-Rückfahrwarner"

Lng.SteeringWheelLOnExit = "Lenkrad beim Aussteigen feststellen."
Lng.BrakesLOnExit = "Bremsen beim Aussteigen aktivieren."
Lng.WheelDust = "Räder verursachen Staub"
Lng.WheelDustWhileBraking = "Staub beim Bremsen"
Lng.MatchPlayerSpeedExit = "Geschwindigkeit beim Aussteigen anpassen"
Lng.NoCollidePlyOnExit = "Keine Kollision für Spieler, die aus einem Fahrzeug aussteigen"
Lng.Exhaust = "Auspuff"
Lng.PassengerSeats = "Beifahrersitze"
Lng.TrailerAttach = "Anhänger aktivieren."
Lng.TrailerAttachConStrengthM = "Anhängerkupplungsstärken-Multiplikator"
Lng.TrailersCanAtchToReg = "Anhänger für PKWs aktivieren."
Lng.RepairToolSpeedMult = "Reparaturgeschwindigkeits-Multiplikator"

if !VC_Lng_T then VC_Lng_T = {} end VC_Lng_T[Lng.Language_Code] = Lng