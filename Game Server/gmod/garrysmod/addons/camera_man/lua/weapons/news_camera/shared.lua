--[[
	Copyright (C) Chessnut - All Rights Reserved
	Unauthorized copying of this file, via any medium is strictly prohibited. Proprietary and confidential.
	Written by Chessnut (chessnutist@gmail.com), December 2014
--]]

SWEP.ViewModel = "models/dav0r/camera.mdl"
SWEP.WorldModel = "models/weapons/w_toolgun.mdl"
SWEP.Spawnable = true
SWEP.AdminOnly = true

SWEP.Primary.Ammo = "none"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1

SWEP.Secondary.Ammo = "none"
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1

function SWEP:Initialize()
	self:SetHoldType("rpg")
end