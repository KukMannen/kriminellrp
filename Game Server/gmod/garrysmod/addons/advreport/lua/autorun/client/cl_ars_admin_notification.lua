net.Receive( "ARS_GotAReport", function()
	local TXT = net.ReadString()
	if ARS.UseSoundNotification then
		surface.PlaySound( ARS.SoundUsed )
	end
	if !IsValid(ARSWarning) and ARS.SendPanelWarning then 
		local timeleft
		timer.Create("TimeOpen", ARS.PanelNotiTimeOpen, 1, function()
			ARSWarning:Remove()
			timer.Remove("TimeOpen")
		end)
		timer.Start("TimeOpen")
		ARSWarning = vgui.Create( "DFrame" ) 
		ARSWarning:SetSize( 300, 100 )
		ARSWarning:SetPos( ScrW()-310, 10 )
		ARSWarning:SetTitle( " " ) 
		ARSWarning:SetVisible( true )
		ARSWarning:SetDraggable( false ) 
		ARSWarning:ShowCloseButton( false ) 				
		ARSWarning.Paint = function()
			draw.RoundedBoxEx( 4, 0, 0, ARSWarning:GetWide(), 30, ARS.RequestHeaderColor, true, true, false, false )
			draw.RoundedBoxEx( 4, 0, 30, ARSWarning:GetWide(), ARSWarning:GetTall()-30, ARS.RequestBGColor, false, false, true, true )	
			draw.SimpleText( "You have received a Report!", "Headings_Text", ARSWarning:GetWide()/2, 2, ARS.TextRequestColor, TEXT_ALIGN_CENTER )
			draw.SimpleText( TXT, "Date_Text", ARSWarning:GetWide()/2, 35, ARS.TextRequestColor, TEXT_ALIGN_CENTER )
			draw.SimpleText( "Type '"..ARS.AdminChatCommand.."' to view!", "Date_Text", ARSWarning:GetWide()/2, 55, ARS.TextRequestColor, TEXT_ALIGN_CENTER )
			draw.SimpleText( math.Round(timeleft), "List_Text", ARSWarning:GetWide()/2, 70, ARS.TextRequestColor, TEXT_ALIGN_CENTER )
		end
		ARSWarning.Think = function()
			timeleft = timer.TimeLeft("TimeOpen")
		end
	end
end)