--Pointshop Betting Main Shared Dist
function BETTING.PlayerIsOnFirstTeam(ply)
	if istable(BETTING.FirstTeam) then
		return BETTING.FirstTeam[4](ply)
	else return ply:Team() == BETTING.FirstTeam end
end

function BETTING.PlayerIsOnSecondTeam(ply)
	if istable(BETTING.SecondTeam) then
		return BETTING.SecondTeam[4](ply)
	else return ply:Team() == BETTING.SecondTeam end
end

function BETTING.GetPlayerTeam(ply)
	if BETTING.PlayerIsOnFirstTeam(ply) then return istable(BETTING.FirstTeam) and BETTING.FirstTeam[1] or BETTING.FirstTeam
	elseif BETTING.PlayerIsOnSecondTeam(ply) then return istable(BETTING.SecondTeam) and BETTING.SecondTeam[1] or BETTING.SecondTeam
	end
end

function BETTING.GetTeamName(t)
	if istable(BETTING.FirstTeam) and (t == BETTING.FirstTeam[1]) then return BETTING.FirstTeam[2]
	elseif istable(BETTING.SecondTeam) and (t == BETTING.SecondTeam[1]) then return BETTING.SecondTeam[2]
	else return team.GetName(t) end
end

function BETTING.GetTeamColor(t)
	if istable(BETTING.FirstTeam) and (t == BETTING.FirstTeam[1]) then return BETTING.FirstTeam[3]
	elseif istable(BETTING.SecondTeam) and (t == BETTING.SecondTeam[1]) then return BETTING.SecondTeam[3]
	else return team.GetColor(t) end
end

function BETTING.IsTeamValid(t)
	if not t then return false end
	if istable(t) and #t == 4 then return true
	elseif istable(t) then return false
	end
	return team.Valid(t)
end

function BETTING.GetTeamID(t)
	if istable(t) then return t[1]
	else return t end
end

function BETTING.GetPointsName()
	return PS and string.lower(PS.Config.PointsName) or "points"
end

function BETTING.GetPoints(ply)
	return PS and ply:PS_GetPoints() or ply:PS2_GetWallet().points 
end

function BETTING.HasPoints(ply, amount)
	if PS then return ply:PS_HasPoints(amount)
	else return (ply:PS2_GetWallet().points >= amount) end
end

function BETTING.GivePoints(ply, amount)
	if PS then ply:PS_GivePoints(amount) else ply:PS2_AddStandardPoints(amount) end
end

function BETTING.TakePoints(ply, amount)
	if PS then ply:PS_TakePoints(amount) else ply:PS2_AddStandardPoints(-amount,false,false,true) end
end

function BETTING.FormatNumber(num)
	num = tonumber(num)
	if (!num) then
		return 0
	end
	if num >= 1e14 then return tostring(num) end
    num = tostring(num)
    sep = sep or ","
    local dp = string.find(num, "%.") or #num+1
	for i=dp-4, 1, -3 do
		num = num:sub(1, i) .. sep .. num:sub(i+1)
    end
    return num
end
