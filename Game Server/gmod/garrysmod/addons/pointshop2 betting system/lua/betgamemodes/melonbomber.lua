--Pointshop betting gamemode scripts are SHARED. Hooks must return the same data on both server and client.
--This script is for the Melonbomber gamemode by Mechanical mind

--Gamemode scripts MUST ALWAYS specify the first and second teams to bet on with these settings:
BETTING.FirstTeam = {2,"Player", Color(0, 204, 0), function(ply) return true end}
BETTING.SecondTeam = {1,"Winner", Color(150, 150, 150), function(ply) return (ply == gmod.GetGamemode().RoundWinner) end}
//You could use this file to load custom settings based on the gamemode e.g.
//BETTING.Settings.MinimumPlayersForBetting = 5
local function PlayerCanBetMelonbomber(ply)
	if !BETTING.Settings.OnlyAllowBetsSelf and !BETTING.Settings.AllowBetsOnOthers then 
		return false,"Team betting mode disabled in Melonbomber!"
	end
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime enabled
	if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
		if BETTING.EndBetsTime and CurTime() <= BETTING.EndBetsTime then return true end
		return false,string.format("You must place a bet in the first %i seconds of the round!",BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
	end
	
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime disabled
	if ply:Alive() then return false,"Players must be dead or spectating to bet." end
	if !gmod.GetGamemode():GetGameState()==2 then return false,"You can't bet whilst a new round is preparing." end
	return true
end
hook.Add("PlayerCanBet","PlayerCanBetMelonbomber",PlayerCanBetMelonbomber)

local OldSetGameState = gmod.GetGamemode().SetGameState
local function SetGameState(gm, state)
	if SERVER then OldSetGameState(gm, state) end
	if SERVER then SendUserMessage("PSBettingOnStartRound", player.GetAll()) end
	BETTING.EndBetsTime = CurTime() + BETTING.Settings.OnlyAllowBettingAtRoundStartTime or 30
end
if CLIENT then usermessage.Hook("PSBettingOnStartRound",SetGameState) end
gmod.GetGamemode().SetGameState = SetGameState

--SERVER hooks
--Every outcome must call BETTING.FinishBets else the bet window will not be closed
if SERVER then
	local OldEndRound = gmod.GetGamemode().EndRound
	local function MelonbomberEndRoundBets(gm,reason,winner)
		OldEndRound(gm,reason,winner)
		if winner then BETTING.FinishBets(1)
		else BETTING.FinishBets(2,true) end
	end
	gmod.GetGamemode().EndRound = MelonbomberEndRoundBets
end