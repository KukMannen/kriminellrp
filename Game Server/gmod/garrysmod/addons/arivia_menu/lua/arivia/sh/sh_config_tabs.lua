Arivia.TabJobs = {
    name = "JOBB",
    description = "PARA ÄR VIKTIGT",
    icon = "arivia/arivia_btn_jobs.png",
    buttonColor = Color(64, 105, 126, 190),
    buttonHoverColor = Color(64, 105, 126, 240),
    unavailableDarken = true,
    unavailableColor = Color( 20, 20, 20, 200 ),
    showUnavailableJobs = true
}
Arivia.TabWeapons = {
    name = "HEM LEVERANS",
    description = "KOMMER MED POSTEN",
    icon = "arivia/arivia_btn_website.png",
    buttonColor = Color( 72, 112, 58, 190 ),
    buttonHoverColor = Color( 72, 112, 58, 240 ),
    unavailableDarken = true,
    unavailableColor = Color( 20, 20, 20, 200 )
}
Arivia.TabAmmo = {
    name = "Ammo",
    description = "Keeping your guns ready",
    icon = "arivia/arivia_btn_ammo.png",
    buttonColor = Color( 163, 135, 79, 190 ),
    buttonHoverColor = Color( 163, 135, 79, 240 ),
    unavailableDarken = true,
    unavailableColor = Color( 20, 20, 20, 200 )
}
Arivia.TabEntities = {
    name = "SVARTMARKNAD",
    description = "GREJOR SNUTEN INTE FÅR SE",
    icon = "arivia/arivia_btn_entities.png",
    buttonColor = Color( 124, 51, 50, 190 ),
    buttonHoverColor = Color( 124, 51, 50, 240 ),
    unavailableDarken = true,
    unavailableColor = Color( 20, 20, 20, 200 )
}
Arivia.TabVehicles = {
    name = "Vehicles",
    description = "Getting your permit",
    icon = "arivia/arivia_btn_vehicles.png",
    buttonColor = Color(64, 105, 126, 190),
    buttonHoverColor = Color(64, 105, 126, 240),
    unavailableDarken = true,
    unavailableColor = Color( 20, 20, 20, 200 )
}
Arivia.TabShipments = {
    name = "LÅDOR",
    description = "MÄNS STORA SKÖNHET",
    icon = "arivia/arivia_btn_shipments.png",
    buttonColor = Color( 145, 71, 101, 190 ),
    buttonHoverColor = Color( 145, 71, 101, 240 ),
    unavailableDarken = true,
    unavailableColor = Color( 20, 20, 20, 200 ),
    showUnavailableGuns = true
}