Arivia.ResourcesEnable = true
Arivia.WorkshopEnabled = true
Arivia.BackgroundsEnable = true
Arivia.Backgrounds = {
"http://178.32.177.208/gmod/images/backgrounds/bg.jpg",
"http://178.32.177.208/gmod/images/backgrounds/bg1.jpg"
}
Arivia.NetworkNameDisplayed = true
Arivia.NetworkName = "Välkommen till KriminellRP"
Arivia.NetworknameColor = Color( 255, 255, 255, 255 )
Arivia.PlayerMoneyColor = Color( 255, 255, 255, 255 )
Arivia.PlayerJobColor = Color( 255, 255, 255, 255 )
Arivia.MiddlePanelBlur = true
Arivia.MiddlePanelBackgroundColor = Color( 16, 16, 16, 210 )
Arivia.LeftMidPanelBlur = true
Arivia.LeftMidPanelBackgroundColor = Color( 0, 0, 0, 250 )
Arivia.LeftTopPanelBackgroundColor = Color( 128, 0, 0, 250 )
Arivia.LeftTopButtonHoverColor = Color( 40, 0, 0, 255 )
Arivia.CloseButtonColor = Color( 255, 255, 255, 255 )
Arivia.InitRegenPanel = false
Arivia.ClockEnabled = true
Arivia.ClockFormat = "%I:%M:%S %p"
Arivia.ClockColor = Color( 255, 255, 255, 255 )
Arivia.LevelSystemEnabled = true
Arivia.UserGroupColors = {}
Arivia.UserGroupColors["superadmin"] = Color( 200, 51, 50, 220 )
Arivia.UserGroupColors["admin"] = Color( 72, 112, 58, 220 )
Arivia.UserGroupColors["supervisor"] = Color( 145, 71, 101, 220 )
Arivia.UserGroupColors["operator"] = Color( 171, 108, 44, 220 )
Arivia.UserGroupColors["moderator"] = Color( 171, 108, 44, 220 )
Arivia.UserGroupColors["trialmod"] = Color( 163, 135, 79, 220 )
Arivia.UserGroupTitles = {}
Arivia.UserGroupTitles["superadmin"] = "Owner"
Arivia.UserGroupTitles["admin"] = "Administrator"
Arivia.UserGroupTitles["supervisor"] = "Supervisor"
Arivia.UserGroupTitles["operator"] = "Moderator"
Arivia.UserGroupTitles["moderator"] = "Moderator"
Arivia.UserGroupTitles["trialmod"] = "Trial Moderator"
Arivia.StaffGroups = { 
    "superadmin", 
    "admin", 
    "moderator" 
}
Arivia.StaffCardBlur = true
Arivia.StaffCardBackgroundUseRankColor = true
Arivia.StaffCardBackgroundColor = Color( 0, 0, 0, 230 )
Arivia.StaffCardNameColor = Color( 255, 255, 255, 255 )
Arivia.StaffCardRankColor = Color( 255, 255, 255, 255 )
Arivia.TruncateEnabled = true
Arivia.TruncateLength = 170
Arivia.BrowserColor = Color( 0, 0, 0, 240 )
Arivia.BCColorServer = Color(255, 255, 0)
Arivia.BCColorName = Color(77, 145, 255)
Arivia.BCColorMsg = Color(255, 255, 255)
Arivia.BCColorValue = Color(255, 0, 0)
Arivia.BCColorValue2 = Color(255, 166, 0)
Arivia.BCColorBind = Color(255, 255, 0)