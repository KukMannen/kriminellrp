DarkRP.createCategory{
    name = "Civila Medborgare",
    categorises = "jobs",
    startExpanded = true,
    color = Color(123, 191, 106, 255),
    canSee = function(ply) return true end,
    sortOrder = 100,
}
DarkRP.createCategory{
    name = "Beredskapstjänst",
    categorises = "jobs",
    startExpanded = true,
    color = Color(16, 78, 139, 255),
    canSee = function(ply) return true end,
    sortOrder = 103,
}
DarkRP.createCategory{
    name = "Gäng",
    categorises = "jobs",
    startExpanded = true,
    color = Color(195, 0, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 101,
}
DarkRP.createCategory{
    name = "Kriminella",
    categorises = "jobs",
    startExpanded = true,
    color = Color(195, 0, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 102,
}
DarkRP.createCategory{
    name = "Regeringen",
    categorises = "jobs",
    startExpanded = true,
    color = Color(16, 78, 139, 255),
    canSee = function(ply) return true end,
    sortOrder = 103,
}
DarkRP.createCategory{
    name = "Annat",
    categorises = "jobs",
    startExpanded = true,
    color = Color(255, 114, 22, 255),
    canSee = function(ply) return true end,
    sortOrder = 104,
}
DarkRP.createCategory{
    name = "Pistoler",
    categorises = "shipments",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 105,
}
DarkRP.createCategory{
    name = "Gevär",
    categorises = "shipments",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 106,
}
DarkRP.createCategory{
    name = "Hagelgevär",
    categorises = "shipments",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 107,
}

DarkRP.createCategory{
    name = "Snipers",
    categorises = "shipments",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 108,
}
DarkRP.createCategory{
    name = "Knivar",
    categorises = "weapons",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 109,
}
DarkRP.createCategory{
    name = "Armor",
    categorises = "weapons",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 109,
}
DarkRP.createCategory{
    name = "Annat",
    categorises = "weapons",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 110,
}
DarkRP.createCategory{
    name = "Jobb Specialare",
    categorises = "entities",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 111,
}
DarkRP.createCategory{
    name = "Pengar skrivare",
    categorises = "entities",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 112,
}
DarkRP.createCategory{
    name = "Drog labb",
    categorises = "entities",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 113,
}
DarkRP.createCategory{
    name = "Första Hjälpen",
    categorises = "entities",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 114,
}
DarkRP.createCategory{
    name = "Ammunition",
    categorises = "entities",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 115,
}
DarkRP.createCategory{
    name = "Vapen Tillbehör",
    categorises = "entities",
    startExpanded = true,
    color = Color(0, 107, 0, 255),
    canSee = function(ply) return true end,
    sortOrder = 116,
}