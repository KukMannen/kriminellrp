ulx showMotd 0
motdfile ulx_motd.txt
ulx chattime 0
ulx meChatEnabled 2
ulx welcomemessage "Välkommen till KriminellRP!" 
ulx logFile 1
ulx logEvents 1
ulx logChat 1
ulx logSpawns 1
ulx logSpawnsEcho 1
ulx logJoinLeaveEcho 1
ulx logDir "ulx_logs"
ulx logEcho 1
ulx logEchoColors 1
ulx logEchoColorDefault "151 211 255"
ulx logEchoColorConsole "0 0 0"
ulx logEchoColorSelf "75 0 130"
ulx logEchoColorEveryone "0 128 128"
ulx logEchoColorPlayerAsGroup 1
ulx logEchoColorPlayer "255 255 0"
ulx logEchoColorMisc "0 255 0"
ulx rslotsMode 0
ulx rslots 0
ulx rslotsVisible 1
ulx votemapEnabled 0
ulx votemapMintime 0
ulx votemapWaittime 0
ulx votemapSuccessratio 0
ulx votemapMinvotes 0
ulx votemapVetotime 0
ulx votemapMapmode 2
ulx voteEcho 0
ulx votemap2Successratio 0
ulx votemap2Minvotes 0
ulx votekickSuccessratio 0.6
ulx votekickMinvotes 2
ulx votebanSuccessratio 0.7
ulx votebanMinvotes 3